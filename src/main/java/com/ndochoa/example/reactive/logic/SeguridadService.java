package com.ndochoa.example.reactive.logic;


import com.ndochoa.example.reactive.entity.Usuario;
import io.reactivex.Maybe;

public interface SeguridadService {

    Maybe<Usuario> crearUsuario(String username);

    Maybe<Boolean> asignarRolAUsuario(String username, String rolName);


}
