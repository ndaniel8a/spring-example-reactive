package com.ndochoa.example.reactive.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Rol {

    @Id
    private Integer id;

    private String name;
}
