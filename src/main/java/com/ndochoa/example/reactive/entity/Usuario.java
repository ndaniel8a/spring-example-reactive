package com.ndochoa.example.reactive.entity;

import lombok.*;
import org.springframework.data.annotation.Id;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Usuario {

    @EqualsAndHashCode.Include
    @Id
    private Long id;


    private String username;


    private Boolean locked;


    private String password;


}
