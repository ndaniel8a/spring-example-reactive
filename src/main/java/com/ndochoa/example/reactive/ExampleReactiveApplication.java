package com.ndochoa.example.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleReactiveApplication.class, args);
    }

}

