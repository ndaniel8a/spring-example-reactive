package com.ndochoa.example.reactive.repository;

import com.ndochoa.example.reactive.entity.Usuario;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends RxJava2CrudRepository<Usuario, Long> {

    Flowable<Usuario> findAll();

    Maybe<Usuario> findByUsername();

}
