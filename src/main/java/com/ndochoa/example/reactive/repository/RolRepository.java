package com.ndochoa.example.reactive.repository;

import com.ndochoa.example.reactive.entity.Rol;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolRepository extends RxJava2CrudRepository<Rol, Long> {
}
